# El Temps - David Duran Cunill

## L'aplicació no es del tot responsive pel que has de fer la finestra més petita per visualitzar-la correctament.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
